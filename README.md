# rescript-notifications

This package closely wraps the web
[Notifications API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API).
It will not include the Notification API Service Worker Additions. Instead,
it will be a dependency of the forthcoming `rescript-service-worker`. 
This package has not been exhaustively tested. Please open issues. 

## Long-term support

This package is currently supported but may go long periods of time without
fresh commits because it is something like "finished". Yes, software is like 
fashion, and fashion is never finished, and I will try to upgrade the ReScript 
language version from time to time, but unless there are significant defects 
in this project you can generally consider this project to be as stable as the 
ReScript language and the Notifications API. 

This is to say: Don't be alarmed by the lack of recent commits! Stability is
good! And don't be afraid to open issues. 

## Installation
`npm install rescript-notifications`

## Implemented
- [X] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent)
- [X] [Notification](https://developer.mozilla.org/en-US/docs/Web/API/Notification)
- [X] [NotificationAction](https://developer.mozilla.org/en-US/docs/Web/API/Notification/actions)
- [X] [NotificationEvent](https://developer.mozilla.org/en-US/docs/Web/API/NotificationEvent)
- [X] [NotificationEventInit](https://developer.mozilla.org/en-US/docs/Web/API/NotificationEvent/NotificationEvent)
- [X] [NotificationOptions](https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification)
- [X] [NotificationPermission](https://developer.mozilla.org/en-US/docs/Web/API/Notification/permission)

## Notes
### 'data
You'll notice these types often have a generic type parameter `'data`. This
represents the `data` property of the NotificationOptions passed into the 
Notification constructor. It can be any structured clonable type, though, not
any type. 

### ExtendableEvent
This interface is technically considered part of the Service Worker API, but 
I've included it here instead to break the circular dependency between 
Service Worker API and Notifications API. In my small universe, 
rescript-service-worker will depend on rescript-notifications, one way. 

### Experimental 🧪 Features
Different browsers implement APIs to varying degrees, and the Notifications
API is no exception. The rule I am following is to implement all Notifications
API features as though they are guaranteed to exist *unless* they are marked
as "Experimental" in MDN with a little picture of a beaker. Those are 
bound as `option`s. If you need to support much older browsers that have far
fewer if any of these API features, this library might not be of much use to
you. Forks welcome. 

## Examples
Read 
[this article](https://webbureaucrat.gitlab.io/posts/displaying-notifications-in-rescript/), 
which documents how to use the binding including a live demo.

## Contributing
I'm a beginner. I'd be surprised if all of this were definitely right. 
[@ me](https://floss.social/@webbureaucrat) or send me a PR if you find 
anything you think I should take a second look at. (Stylistically, though, 
I'll warn you I'm pretty
dug in, at least for now)

## License

Because I have merely translated the API from the JavaScript specification
into ReScript with no implementation, I do not consider anything in this
repository to be copyrightable. 

As a courtesy, I have included a copy of the MIT license anyway just in case 
your lawyers are fussy about this sort of thing. 
