type t<'data>;

@new
external makeWithOptions: (string, NotificationOptions.t<'data>) => t<'data>
  = "Notification";

@new external makeWithoutOptions: string => t<'data> = "Notification";

/* static properties */

@scope("Notification") @val external permission: string = "permission";

@scope("Notification") @val external maxActions: option<float> = "maxActions";

/* instance properties */
@get
external actions: t<'data> => option<array<NotificationAction.t>>
  = "actions";

@get external badge: t<'data> => string = "badge";
@get external body: t<'data> => string = "body";
@get external data: t<'data> => Js.Nullable.t<'data> = "data";
@get external dir: t<'data> => NotificationOptions.dir = "dir";
@get external lang: t<'data> => string = "lang";
@get external tag: t<'data> => string = "tag";
@get external icon: t<'data> => string = "icon";
@get external image: t<'data> => option<string> = "image";
@get external renotify: t<'data> => option<bool> = "renotify";
@get external requireInteraction: t<'data> => bool = "requireInteraction";
@get external silent: t<'data> => bool = "silent";
@get external timestamp: t<'data> => float = "timestamp";
@get external title: t<'data> => string = "title";
@get external vibrate: t<'data> => option<array<float>> = "vibrate";

/* handlers */

type mouseHandler = Dom.mouseEvent => unit
@get external get_onclick: t<'data> => mouseHandler = "onclick"
@set external set_onclick: (t<'data>, mouseHandler) => unit = "onclick"

type handler = Dom.event => unit
@get external get_onclose: t<'data> => handler = "onclose"
@set external set_onclose: (t<'data>, handler) => unit = "onclose"

@get external get_onerror: t<'data> => handler = "onerror"
@set external set_onerror: (t<'data>, handler) => unit = "onerror"

@get external get_onshow: t<'data> => handler = "onshow"
@set external set_onshow: (t<'data>, handler) => unit = "onshow"

type handler_like<'a> = 'a => unit //TODO this could be more specific
@send
external addEventListener: (t<'data>, string, handler_like<'a>) => unit
  = "addEventListener"

/* static methods */
@scope("Notification") @val
external requestPermission: unit => promise<string> = "requestPermission"

/* instance methods */
@send external close: t<'data> => unit = "close"
