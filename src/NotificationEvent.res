type _notificationEvent<'a>;
type notificationEvent_like<'a> = ExtendableEvent_like(_notificationEvent<'a>);
type t<'data> = notificationEvent_like<Dom._baseClass>;

@send
external makeWithOptions: (string, NotificationEventInit.t<'data>) => t<'data>
  = "make";

@send external makeWithoutOptions: string => t<'data> = "make";

@get external notification: t<'data> => Notification.t<'data> = "notification";
@get external action: t<'data> => string = "action";
