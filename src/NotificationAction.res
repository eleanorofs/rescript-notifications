type t;

@get external action: t => string = "action";
@get external title: t => string = "title";
@get external icon: t => string = "icon";
