type _t<'a>;
type t_like<'a> = Dom.event_like<_t<'a>>;
type t = t_like<Dom._baseClass>;

@send
external waitUntil: (t_like<'subtype>, promise<'a>) => unit
  = "waitUntil";
